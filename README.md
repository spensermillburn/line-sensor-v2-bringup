# line-sensor-v2-bringup
A standalone utility for testing the alphabot-v2 line-sensor. 


## The what

This repository contains a data visualization of data captured from the alphabot-2.0 line-sensor bringup effort. 

## The why

A new driver is required as the previous line sensor featured and STM32 device to handle converting hall sensor SPI information to uart. Bot 2.0 will interface directly from qnx to a 16 channel ADC on the line-sensor PCB

## The how

Data was captured via a standalone script "linefollower.c"

To compile and and run this script, source the qnx71 toolchain on your local machine, and execute
```
ntoaarch64-gcc linefollower.c -o linefollower -lspi-master
scp linefollower root@{target_ip_addr}:/opt
#ssh into target and /opt/linefollower | tee output_data.txt
```

After using SCP to collect the output, put it in the data folder of this repository and launch the jupyter notebook with
```
python3 -m notebook line-follower-v2-bringup.ipynb 
```
edit the data path in the first cell to use your new file.
Run all cells in the notebook to see plots in adc-counts, millivolts, gauss, milliteslas and microteslas

