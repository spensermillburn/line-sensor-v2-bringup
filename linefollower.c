#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/resmgr.h>
#include <sys/neutrino.h>
#include <hw/inout.h>
#include <hw/nicinfo.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <inttypes.h>
#include <string.h>
#include <errno.h>
#include <devctl.h>
#include <hw/spi-master.h>
#include <sys/time.h>



int spi_setDevCfg(char* name, int id, spi_cfg_t*   pCfg)
{
	int			fd;
    	/*
     	 * Open
     	*/

	if ((fd = spi_open(name)) < 0) 
	{
		fprintf(stderr, "SPI open failed, fd = %d errno = %d\n", fd, errno);
		strerror(errno);
		return(-1);
	}

    	if (spi_setcfg(fd, id, pCfg) != EOK)
    	{
		fprintf(stderr, "Shouldn't have received error from cfg\n");
        	spi_close(fd);
		return(-1);
    	}
  
    	
    	spi_close(fd);
    
    	return 0;
}



int my_getdev()
{
        spi_devinfo_t   info;
        int                             i, fd, dev;

        fprintf(stderr, "\n########### spi_getdevinfo() test ###########\n");

        /*
         * Open
         */
        if ((fd = spi_open("/dev/spi1")) < 0) 
	{
                fprintf(stderr, "SPI open failed, fd = %d errno = %d\n", fd, errno);
                strerror(errno);
                return(-1);
        }


        fprintf(stderr, "\n----- Get dev 1 info -----\n\n");
        dev = 1;
        fprintf(stderr, "----- get  device 0x%04x info -----\n", dev);

                i = spi_getdevinfo(fd, dev, &info);
                if (i == EOK) 
		{
                        fprintf(stderr, "device     = %x\n", info.device);
                        fprintf(stderr, "name       = %s\n", info.name);
                        fprintf(stderr, "mode       = %x\n", info.cfg.mode);
                        fprintf(stderr, "clock_rate = %d\n", info.cfg.clock_rate);
                        dev = info.device & SPI_DEV_ID_MASK;
                }
                return 0; 
}

void current_timestamp() {
    struct timeval te; 
    gettimeofday(&te, NULL); // get current time
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // calculate milliseconds
    printf("time in millis: %lld\n", milliseconds);
}

int lfollow(void)
{
    int                 i,fd,idx;
    char*       devname = "/dev/spi1";
    int         devid   = 0;
    uint32_t    myBuffer[100];
    uint32_t    rxBuffer[100];
    uint32_t    temp;
    uint32_t    base;
    uint8_t*    pPtr;


    spi_cfg_t       cfg;
    cfg.mode=0x408;               // = ((8 & SPI_MODE_CHAR_LEN_MASK) | SPI_MODE_BODER_MSB | SPI_MODE_CKPOL_HIGH);
    cfg.clock_rate  = 2500000;
    spi_setDevCfg(devname,devid, &cfg);


    if ((fd = spi_open(devname)) < 0)
    {
            fprintf(stderr, "SPI open failed, fd = %d errno = %d\n", fd, errno);
            strerror(errno);
            return(-1);
    }


    // Reset ADC device ts
    pPtr = (uint8_t*)myBuffer;
    *pPtr++ = 0x42;
    *pPtr++ = 0x00;


    i = spi_xchange(fd, devid, (uint8_t*)myBuffer, (uint8_t*)rxBuffer, 4);


    // Select last channel (15) in canning sequence
    pPtr = (uint8_t*)myBuffer;
    *pPtr++ = 0x97;
    *pPtr++ = 0xc0;

    i = spi_xchange(fd, devid, (uint8_t*)myBuffer, (uint8_t*)rxBuffer, 4);



    // Auto-2 mode, prog D10-0, reset to chan 0
    pPtr = (uint8_t*)myBuffer;
    *pPtr++ = 0x3c;
    *pPtr++ = 0x00;


    i = spi_xchange(fd, devid, (uint8_t*)myBuffer, (uint8_t*)rxBuffer, 4);


    // Auto-2 mode
    pPtr = (uint8_t*)myBuffer;
    *pPtr++ = 0x38;
    *pPtr++ = 0x40;


    i = spi_xchange(fd, devid, (uint8_t*)myBuffer, (uint8_t*)rxBuffer, 4);


    // Auto-2 mode
    pPtr = (uint8_t*)myBuffer;
    *pPtr++ = 0x30;
    *pPtr++ = 0x0;


    i = spi_xchange(fd, devid, (uint8_t*)myBuffer, (uint8_t*)rxBuffer, 4);



    // ***************************************************************************
    // Congiguration complete, read and display conversions of the 16 ADC channels
    // ***************************************************************************



   for (idx=0; idx < 16; idx++)
   {
   	pPtr = (uint8_t*)myBuffer;
    	*pPtr++ = 0x00;
    	*pPtr++ = 0x00;
    	i = spi_xchange(fd, devid, (uint8_t*)myBuffer, (uint8_t*)rxBuffer, 4);
		
    	pPtr = (uint8_t*)rxBuffer;
	int ch = (uint32_t)*pPtr++;
		
	printf("Channel %02d -",  (ch & 0xf0) >> 4);

	// Form the 12 bits of the conversion and display
      printf("0x%03x",  ((ch & 0x0f) << 6) | ((uint32_t)*pPtr & 0xfc) >> 2);

	printf("\n");

    }


        current_timestamp();
        return 0; 

}

int main(int argc, char *argv[]){
  for(int i=0; i<1000;i++){
  usleep(5000);
  int x = lfollow();
  }

}
